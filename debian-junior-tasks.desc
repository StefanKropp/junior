Task: debian-junior
Relevance: 7
Section: debian-junior
Description: Debian Junior Pure Blend
 .

Task: junior-art
Parent: debian-junior
Section: debian-junior
Description: Debian Jr. Art
 Tools for children to produce artwork.  The simplest of these is
 tuxpaint, which is designed for small children.  It features sounds
 and easy to use controls.  For older children, there are gimp and
 xpaint.  While gimp is more complex, and is aimed at  the graphic
 artist, it is not necessarily much harder to use than the older,
 less full-featured xpaint.  Children starting out with just a small
 subset of the functions of these tools eventually pick up quite a
 number of new things as they explore, either with adults or other
 children with whom they share their computers, or on their own.
Key:
 junior-art

Task: junior-education
Parent: debian-junior
Section: debian-junior
Description: Debian Jr. education applications
 This package includes a collection of educational programs
 for children. These applications suit children from 2 to 12.
 They will learn the computer mouse and keyboard as
 well as various skills (numbers, letters, logic, etc.).
Key:
 junior-education

Task: junior-games-adventure
Parent: debian-junior
Section: debian-junior
Description: Debian Jr. Adventure Games
 Adventure games are interactive stories driven by exploration and puzzle
 solving, and can be quite challenging. Recommended for older children, maybe
 even requiring help from someone older.
Key:
 junior-games-adventure

Task: junior-games-arcade
Parent: debian-junior
Section: debian-junior
Description: Debian Jr. arcade games
 This metapackage will install arcade games suitable, in the
 opinion of the contributors to the Debian Jr. project, for children
 of all ages.  Mastering most of these games requires dexterity
 and cognitive skills usually developed only in the older children
 (around ages 5 to 8).  But the youngest children may enjoy watching
 older players, or just poking randomly at the controls, depending
 on the game.
Key:
 junior-games-arcade

Task: junior-games-card
Parent: debian-junior
Section: debian-junior
Description: Debian Jr. Card Games
 A collection of card games, for the time being just solitaire, which
 are probably more suited to older children than young, although
 the "memory" type games in pysol may appeal to children just entering
 grade school.
Key:
 junior-games-card

Task: junior-games-gl
Parent: debian-junior
Section: debian-junior
Description: Debian Jr. 3D Games (hardware acceleration required)
 These games all use OpenGL libraries.  They will not work without
 decent 3D graphics cards providing hardware-accelerated OpenGL.
 .
 While armagetron and gltron are different interpretations of the
 game depicted in the classic 3D-animated movie "Tron", Tux Racer
 is an all-original made-for-Linux 3D racing game.  Unlike the
 standard blood-and-guts fare in the 3D games universe, all of
 these games are suitable for children.
Key:
 junior-games-gl

Task: junior-games-net
Parent: debian-junior
Section: debian-junior
Description: Debian Jr. Network Games
 Network games bring people together from all over the world. Many times the
 game and chat is a place where people learn languages, how to communicate, and
 socializing skills.
Key:
 junior-games-net

Task: junior-games-puzzle
Parent: debian-junior
Section: debian-junior
Description: Debian Jr. Puzzle games
 Some puzzle-type games, from the more arcade-like and frozen-bubble to xjig (a
 jigsaw puzzle program), to lmemory (based on the classic "memory" card game).
 This sampling of packages was done in the hope that it will appeal to child
 and adult alike.
Key:
 junior-games-puzzle

Task: junior-games-sim
Parent: debian-junior
Section: debian-junior
Description: Debian Jr. Simulation Games
 Simulation games tend to be rather complex, so they are recommended
 for older children, and even then some help from someone older may be
 needed.
Key:
 junior-games-sim

Task: junior-games-text
Parent: debian-junior
Section: debian-junior
Description: Debian Jr. Text Games
 The principal virtue of these games is their simplicity.  They will run
 on any hardware, and some of them are simple enough to control so that
 very young children can enjoy them (e.g. snake)
Key:
 junior-games-text

Task: junior-internet
Parent: debian-junior
Section: debian-junior
Description: Debian Jr. Internet tools
 For children, a wide variety of Internet tools are not necessary to get
 started.  Most users will find the Iceweasel web browser covers their needs.
 As children's familiarity and sophistication of use of the Internet develops,
 you will probably want to add more Internet clients to the child's system.
Key:
 junior-internet

Task: junior-math
Parent: debian-junior
Section: debian-junior
Description: Debian Jr. educational math
 This metapackage will install educational math programs suitable for
 children.  Some of the packages use mathematics that is well beyond
 the abilities of young children (e.g. fractals and cryptography), but
 hopefully using these let them gain an appreciation of the beauty of
 math from an early age.  Other packages allow children to explore and
 learn math concepts in an engaging, interactive way.  Some packages
 are more general, providing math activities as only one part of the
 package, e.g. bsdgames includes "arithmetic" in addition to other
 non-math games, and x11-apps provides xcalc.
Key:
 junior-math

Task: junior-programming
Parent: debian-junior
Section: debian-junior
Description: Debian Jr. programming
 The emphasis for this sampling of programming packages for children
 is first on simple interpreted languages.  Also important for using
 languages with children are good documentation, and some ability to
 produce visual programs without too much effort.  It is by no means
 a complete list, and you are encouraged to explore other languages
 with children as well; even those that are not specifically aimed for
 children.
 .
 The Logo language is specifically designed for children, with a strong
 emphasis on the visual and concrete.  Littlewizard is an icon-based
 graphical programming language also designed for children.
 .
 Scratch is an easy, interactive, collaborative, visual programming language
 with children from ages 8 and up as their target group.
 .
 Arduino is a popular micro controller that is widely used by both people
 learning electronics and programming as well as seasoned veterans doing
 complex projects. It is necessary to connect a physical Arduino circuit board
 in order to experiment with arduino.
Key:
 junior-programming

Task: junior-sound
Parent: debian-junior
Section: debian-junior
Description: Debian Jr. sound
 This metapackage will install a sampling of sound packages for
 a system that is used by children.  While not all of these will
 be things that children will use directly, a child's system
 administrator can make use of them to ensure that the child
 gets the most out of their system.
Key:
 junior-sound

Task: junior-system
Parent: debian-junior
Section: debian-junior
Description: Debian Jr. System tools
 This package includes a few tools for helping children learn about and
 use their system.
 .
 Midnight Commander is more than just a file manager.  It is shell with
 which children can explore and manage their own accounts.  It can be a
 great help for the very young, as it saves them some typing (as
 compared with using a command-line shell directly).  It is also packed
 with power and flexibility which can be tapped by older children and
 adults alike.
 .
 The 'hello' sample program can be used as an instructional aid, as
 an example of how to invoke programs from the shell, specify switches,
 use the man page, and so on.
 .
 Baobab, the GNOME disk usage analyzer, enables the user to take a
 visual trip into the hard drive contents. It can be a mesmerizing first
 step, dvelving into the inner parts of the computer system. It serves
 both as an educational tool about file systems and file contents/size
 as well as a tool for analyzing what files are big if disk space is
 scarce.
 .
 Bubblefishymon is a computer load monitor dockapp which shows network
 traffic as fishes, CPU usage as bubbles, and a duck representing a
 duck.
Key:
 junior-system

Task: junior-toys
Parent: debian-junior
Section: debian-junior
Description: Debian Jr. desktop toys
 This metapackage will install desktop toys suitable for children.  The
 collection contains some packages which might enjoy children and make
 them love their computer.
Key:
 junior-toys

Task: junior-typing
Parent: debian-junior
Section: debian-junior
Description: Debian Jr. typing
 This metapackage will install typing tutors and typing games for
 various skill levels.  This collection of packages was assembled
 in the hope that children become comfortable with the keyboard
 quickly while having fun learning it.
Key:
 junior-typing

Task: junior-video
Parent: debian-junior
Section: debian-junior
Description: Debian Jr. Video
 Tools to watch video files, DVD, CD, network streams etc.
Key:
 junior-video

Task: junior-writing
Parent: debian-junior
Section: debian-junior
Description: Debian Jr. writing
 This metapackage will install tools for your budding young
 writer. It includes a text editor, spell-checker, dictionary client,
 word processor, and educational writing software.
Key:
 junior-writing

